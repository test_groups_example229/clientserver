import socket

# Create a socket object
server_socket = socket.socket()
print("Socket successfully created")

# Define the IP address and port to bind
ip = "192.168.1.9"
port = 12345

# Bind the socket to the IP and port
server_socket.bind((ip, port))
print("Socket binded to %s:%s" % (ip, port))

# Put the socket into listening mode
server_socket.listen(5)
print("Socket is listening")

# Read content from file and send them into the socket until EOF
file_path = r"C:\Users\User\Desktop\อ.ภูสิต\client-server\a.txt"
with open(file_path, "r") as file:
    file_contents = file.read()
    while True:
        client_socket, addr = server_socket.accept()
        print('Got connection from', addr)

        # Send the contents of the file to the client
        client_socket.send(file_contents.encode())

        # Close the connection with the client
        client_socket.close()

        # Break the loop as we only need to send the file once
        # break